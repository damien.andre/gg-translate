;; -----------------------------------------------------
;; COPYING
;; -----------------------------------------------------
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;  This program is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.
;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <http://www.gnu.org/licenses/>.


;; -----------------------------------------------------
;; GOOGLE TRANSLATE : M-x gg-translate
;; -----------------------------------------------------
;; This script uses the goslate python module. To use this script, you must install python-pip and goslate. 
;; For debian based distribution, simply type : 'sudo apt-get install python-pip' and 'sudo pip install goslate'
;; To use it, select the text to traduct and type 'M-x gg-translate'  
;; To install it, add the following lines to your .emacs file

(setq default-gg-lang "fr") 
;; This set the default lang. To get all the available lang and their keys, type in a python console :
;; >>> import goslate
;; >>> gs = goslate.Goslate()
;; >>> gs.get_languages()

(setq python-pip-dir "/usr/local/lib/python2.7/dist-packages/") 
;; This set your python pip dir. To get it, you can ask python with :
;; >>>  import site; site.getsitepackages()

(defun gg-translate (beg end lang)
  (interactive (if (use-region-p)
                   (list 
		        (region-beginning) 
			    (region-end) 
			        (read-string (format "Lang (en, fr... default is %s) ? " default-gg-lang)))
                   (list nil nil nil)))
  (if (equal "" lang)
      (setq lang default-gg-lang))
  (setq default-gg-lang lang)

  (if beg
      (progn (setq target (buffer-substring beg end))
	     (setq my-buffer (get-buffer-create "*translate*"))
	     (pop-to-buffer my-buffer)
	     (setq target (replace-regexp-in-string "\n" " " target))
	     (setq cmd (concatenate 'string "echo " "\"" target "\"" " | python " python-pip-dir "goslate.py -t " lang))
	     (setq translation (shell-command-to-string cmd))
	     (insert target)
	     (insert "\n--->\n")
	     (insert translation))
    (message "you must select a region"))
  )